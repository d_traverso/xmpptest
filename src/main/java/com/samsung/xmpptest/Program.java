/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samsung.xmpptest;

import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;

/**
 *
 * @author dario
 */
public class Program {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Create XMPP connection to gmail.com server
        XMPPConnection connection = new XMPPConnection("gmail.com");

        try {
            // Connect
            connection.connect();

            // Login with appropriate credentials
            connection.login("sammysamsung@gmail.com", "apsl135!");

            // Get the user's roster
            Roster roster = connection.getRoster();

            // Print the number of contacts
            System.out.println("Number of contacts: " + roster.getEntryCount());

            // Enumerate all contacts in the user's roster
            for (RosterEntry entry : roster.getEntries()) {
                System.out.println("User: " + entry.getUser());
            }
        } catch (XMPPException e) {
            // Do something better than this!
            e.printStackTrace();
        }
    }
    
}
